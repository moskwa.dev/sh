# Define variables
# Interactive mode
interactive_mode=true
# Detect if the script is run in a non-interactive shell
# Check for both
# if [[ $- == *i* ]]; then
# if [ -t 0 ]; then
if [[ $- == *i* ]] || [ -t 0 ]; then
  echo "Script is being run interactively"
  interactive_mode=true
else
  echo "Script is being run non-interactively. Consider downloading the script and running it locally."
  interactive_mode=false
fi

# Get the first argument of the script
# If no argument is provided, use 0
script_arguments=("$@")
# Print the first

# Define functions
create_wordpress_project() {
  # If the script is run in interactive mode, prompt the user for input
  # Otherwise, use the default value
  input_text=""
  if [ "$interactive_mode" = true ]; then
    # Ask for user input for the name of the project
    read -p "Enter the name of the project: " input_text
  else
    input_text=${script_arguments[1]:-0}
  fi
  git clone https://gitlab.com/moskwa.dev/templates/wp-template.git $input_text
  # Enter the directory and remove the git folder
  cd $input_text
  rm -rf .git
  cd ..
  break
}

create_python_poetry_project() {
  # Ask for user input for the name of the project
  input_text=""
  if [ "$interactive_mode" = true ]; then
    # Ask for user input for the name of the project
    read -p "Enter the name of the project: " input_text
  else
    input_text=${script_arguments[1]:-0}
  fi
  git clone https://gitlab.com/moskwa.dev/templates/python-poetry-template.git $input_text
  # Enter the directory and remove the git folder
  cd $input_text
  rm -rf .git
  cd ..
  break
  # Add more commands here for function 1
}

list_available_options() {
  echo "Available options:"
  echo "0) Help"
  echo "1) Create WordPress project"
  echo "2) Create Python Poetry project"
  # ... More options
}

help() {
  echo "This is a help function."
  # List the available options
  list_available_options
  # ... More options
}

# Function to display menu and handle user input
prompt_user_and_execute() {
  # If the script is run in interactive mode, prompt the user for input
  # Otherwise, use the default value
  if [ "$interactive_mode" = true ]; then
    list_available_options
    # Read user choice
    read -p "Enter your choice: " choice
  else
    # The choice is the first argument of the script or 0
    choice=${script_arguments[0]:-0}
  fi
  # ... More menu options

  # Call function based on user choice
  case $choice in
    help) help ;;
    wp) create_wordpress_project ;;
    poetry) create_python_poetry_project ;;
    *)
      echo "Invalid choice"
      exit 1
      ;;
  esac
}

# Execute the prompt function
prompt_user_and_execute
