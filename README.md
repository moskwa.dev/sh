## Usage

When piping the script from a terminal, pass the parameters as follows

```bash
cat init.sh | sh -s -- 1
```
